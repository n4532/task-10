#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string.h>
#include "Adress.h"
#include "soldier.h"

using namespace std;

Soldier::Soldier()
{
}

Soldier::~Soldier()
{
}

void Soldier::setFirstName(char* s)
{
	strcpy(firstName, s);
}

void Soldier::setSecondName(char* s)
{
	strcpy(secondName, s);
}

void Soldier::setNationality(char* s)
{
	strcpy(nationality, s);
}

void Soldier::setRank(char* s)
{
	strcpy(rank, s);
}

void Soldier::setBirthday(int day, int month, int year)
{
	birthday[0] = day;
	birthday[1] = month;
	birthday[2] = year;
}

void Soldier::setAdress(int post, char* cntr, char* reg, char* dist, char* city, char* strt, int hse, int flt)
{
	Adress adr(post, cntr, reg, dist, city, strt, hse, flt);
	this->address = adr;
	changeAdressStatus();
}

void Soldier::setSoldier(char* fst, char* scnd, char* natn, char* rnk, int d, int m, int y)
{
	setFirstName(fst);
	setSecondName(scnd);
	setNationality(natn);
	setRank(rnk);
	setBirthday(d, m, y);
}

char* Soldier::getFirstName()
{
	return firstName;
}

char* Soldier::getSecondName()
{
	return secondName;
}

char* Soldier::getRank()
{
	return rank;
}

char* Soldier::getNationality()
{
	return nationality;
}

int Soldier::getYear()
{
	return birthday[2];
}

void Soldier::displaySoldier()
{
	cout << endl << firstName << " " << secondName << "," << endl;
	cout << nationality << ", " << rank << "." << endl;
	cout << this->birthday[0] << "-" << this->birthday[1] << "-" << this->birthday[2] << endl;

	if (checkAdressStatus()) {
		this->displayAdress();
	}
}

void Soldier::displayAdress() {
	address.displayAdress();
}

bool Soldier::checkAdressStatus()
{
	return hasAdressStatus;
}

void Soldier::changeAdressStatus()
{
	if (!checkAdressStatus()) {
		hasAdressStatus = true;
	}
	else {
		hasAdressStatus = false;
	}
}
