#pragma once
#define soldier_h
#define Adress_h

//const int N = 20;

class Adress
{
private:
	char country[20], region[20], district[20], city[20], street[20];
	int house, flat, postIndex;
public:
	Adress();
	Adress(int, char*, char*, char*, char*, char*, int, int);
	virtual void setAdress(int, char*, char*, char*, char*, char*, int, int);
	void setCountry(char*);
	void setRegion(char*);
	void setDistrict(char*);
	void setCity(char*);
	void setStreet(char*);
	void setHouse(int);
	void setFlat(int);
	void setPostIndex(int);
	char* getCountry();
	char* getRegion();
	char* getDistrict();
	char* getCity();
	char* getStreet();
	int getHouse();
	int getFlat();
	int getPostIndex();
	virtual void displayAdress();
};

