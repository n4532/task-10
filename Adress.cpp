#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string.h>
#include "Adress.h"
#include "soldier.h"

using namespace std;

Adress::Adress()
{
}

Adress::Adress(int postIndex, char* country, char* region, char* district, char* city, char* street, int house, int flat)
{
	this->setAdress(postIndex, country, region, district, city, street, house, flat);
}

void Adress::setAdress(int postIndex, char* country, char* region, char* district, char* city, char* street, int house, int flat)
{
	setCountry(country);
	setRegion(region);
	setDistrict(district);
	setCity(city);
	setStreet(street);
	setHouse(house);
	setFlat(flat);
	setPostIndex(postIndex);
}

void Adress::setCountry(char* s)
{
	strcpy(country, s);
}

void Adress::setRegion(char* s)
{
	strcpy(region, s);
}

void Adress::setDistrict(char* s)
{
	strcpy(district, s);
}

void Adress::setCity(char* s)
{
	strcpy(city, s);
}

void Adress::setStreet(char* s)
{
	strcpy(street, s);
}

void Adress::setHouse(int n)
{
	house = n;
}

void Adress::setFlat(int n)
{
	flat = n;
}

char* Adress::getCountry()
{
	return country;
}

char* Adress::getRegion()
{
	return region;
}

char* Adress::getDistrict()
{
	return district;
}

char* Adress::getCity()
{
	return city;
}

char* Adress::getStreet()
{
	return street;
}

int Adress::getHouse()
{
	return house;
}


int Adress::getFlat()
{
	return flat;
}


void Adress::setPostIndex(int n)
{
	postIndex = n;
}


int Adress::getPostIndex()
{
	return postIndex;
}


void Adress::displayAdress()
{
	cout << getCountry() << ", ";
	cout << getRegion() << ", ";
	cout << getDistrict() << ", ";
	cout << getCity() << ", ";
	cout << getStreet() << ", ";
	cout << getHouse() << ", ";
	cout << getFlat() << ". \t";
}
