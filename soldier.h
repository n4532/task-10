#pragma once
//#ifndef soldier_h
#define soldier_h
#define Adress_h

//const int N = 20;
//const int M = 3;

class Soldier
{
	public:
		Soldier();
		~Soldier();
		void setFirstName(char*);
		void setSecondName(char*);
		void setNationality(char*);
		void setRank(char*);
		void setBirthday(int, int, int);
		void setAdress(int, char*, char*, char*, char*, char*, int, int);
		void setSoldier(char*, char*, char*, char*, int, int, int);
		char* getFirstName();
		char* getSecondName();
		char* getRank();
		char* getNationality();
		int getYear();
		void displaySoldier();
		void displayAdress();
	private:
		char firstName[20], secondName[20], rank[20], nationality[20];
		int birthday[3];
		bool hasAdressStatus = false;
		bool checkAdressStatus();
		void changeAdressStatus();
		Adress address;
};

