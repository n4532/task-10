#define _CRT_SECURE_NO_WARNINGS
#include <io.h>
#include <iostream>
#include <fstream>
#include "Adress.h"
#include "soldier.h"

using namespace std;

//Soldier* initSoldiers(int);
//Soldier initSoldier();
//void displaySoldiers(Soldier*, int);
//void displayChoise(Soldier*, int, char*, int, int);
//void sortNationality(Soldier*, int);
//void swap(Soldier&, Soldier&);

Soldier initSoldier();
void InitFile(char*);
void displayBinaryFile(char*);
void AddToEndFile(char*);
void ChangeFile(char*);
void RemoveFromFile(char*);
void InitNumber(long&);
void SystemFun();
void InitNameFile(char*);
void SortingFile(char*);
bool SortRanks(char*, char*);
int Menu();

const int N = 20;

int main()
{
	char fileName[N] = "List.txt";
	enum { Init = 1, Create, Display, Add, Change, Remove, Sort, Exit };
	while (true)
	{
		int key = Menu();
		if (key == Exit)
			return 0;
		system("cls");
		switch (key)
		{
		case Init:
			InitNameFile(fileName);
			SystemFun();
			break;
		case Create:
			InitFile(fileName);
			SystemFun();
			break;
		case Display:
			displayBinaryFile(fileName);
			SystemFun();
			break;
		case Add:
			AddToEndFile(fileName);
			SystemFun();
			break;
		case Change:
			ChangeFile(fileName);
			SystemFun();
			break;
		case Remove:
			RemoveFromFile(fileName);
			SystemFun();
			break;
		case Sort:
			SortingFile(fileName);
			SystemFun();
			break;
		default:
			cout << "\nIncorrect input! Try again!";
			SystemFun();
		}
	}
}

void InitFile(char* fileName)
{
	ofstream streamOut;
	streamOut.open(fileName, ios::binary);
	if (!streamOut.is_open())
	{
		cout << "\nCan't open file to write!";
		SystemFun();
		return;
	}
	int bufSize = sizeof(Soldier);
	Soldier* soldier = new Soldier;  
	char ok = 'y';
	while (ok == 'y')
	{
		*soldier = initSoldier();
		streamOut.write((char*)soldier, bufSize);
		cout << " If do you want to continue, press 'y' :";
		cin >> ok;
		system("cls");
	}
	streamOut.close();
}

void AddToEndFile(char* fileName)
{
	ofstream streamOut(fileName, ios::app | ios::binary);
	if (!streamOut.is_open())
	{
		cout << "Can't open file to write!";
		SystemFun();
		return;
	}
	int bufSize = sizeof(Soldier);
	Soldier soldier;
	char Ok = 'y';
	while (Ok == 'y')
	{
		soldier = initSoldier();
		streamOut.write((char*)&soldier, bufSize);
		cout << " If do you want to continue, press 'y' : ";
		cin >> Ok;
		system("cls");
	}
	streamOut.close();
}

void ChangeFile(char* fileName)
{
	fstream streamInOut(fileName, ios::in | ios::out | ios::binary);
	if (!streamInOut)
	{
		cout << "Can't open file to read and write!";
		SystemFun();
		return;
	}
	int bufSize = sizeof(Soldier);
	Soldier soldier;
	long position;
	InitNumber(position);
	streamInOut.seekp((position - 1) * bufSize, ios::beg);
	soldier = initSoldier();
	streamInOut.write((char*)&soldier, bufSize);
	streamInOut.close();
}

void RemoveFromFile(char* fileName)
{
	fstream streamInOut(fileName, ios::in | ios::out | ios::binary);
	if (!streamInOut.is_open())
	{
		cout << "Can't open file to read and write!";
		SystemFun();
		return;
	}
	streamInOut.seekp(0, ios::end);//*
	long n = streamInOut.tellp();//*
	int bufSize = sizeof(Soldier);
	Soldier soldier;
	long Position;
	InitNumber(Position);
	streamInOut.seekp(Position * bufSize, ios::beg);
	while (streamInOut.read((char*)&soldier, bufSize))
	{
		//streamInOut.seekp( (Position - 1 + i ) * bufSize, ios::beg);
		//streamInOut.write((char*)&man, bufSize);
		//streamInOut.seekp( bufSize, ios::cur);
		//i++;
		streamInOut.seekp(-2 * bufSize, ios::cur);
		streamInOut.write((char*)&soldier, bufSize);
		streamInOut.seekp(bufSize, ios::cur);
	}
	streamInOut.close();
	int diskriptorFile = _open(fileName, 2);//*
	_chsize(diskriptorFile, n - bufSize);//*
	_close(diskriptorFile);//*
}

// Peredelajem


void SortingFile(char* fileName)
{
	bool flag = true;
	while (flag)
	{
		fstream streamInOut(fileName, ios::in | ios::out | ios::binary);
		if (!streamInOut.is_open())
		{
			cout << "Can't open file to read and write!";
			SystemFun();
			return;
		}
		flag = false;
		Soldier soldierOne, soldierTwo;
		int bufSize = sizeof(Soldier);
		streamInOut.read((char*)&soldierOne, bufSize);
		while (streamInOut.read((char*)&soldierTwo, bufSize))
		{
			if (SortRanks(soldierTwo.getRank(), soldierOne.getRank()))
			{
				streamInOut.seekp(-2 * bufSize, ios::cur);
				streamInOut.write((char*)&soldierTwo, bufSize);
				streamInOut.write((char*)&soldierOne, bufSize);
				flag = true;
			}
			streamInOut.seekp(-bufSize, ios::cur);
			streamInOut.read((char*)&soldierOne, bufSize);
		}
		streamInOut.close();
	}
}

bool SortRanks(char* firstRank, char* secondRank)
{
	const int N = 25;
	const int M = 6;
	char** ranks = new char*[M];
	char* rank1 = new char[N] {"Private"};
	ranks[0] = rank1;
	char* rank2 = new char[N] {"Sergeant"};
	ranks[1] = rank2;
	char* rank3 = new char[N] {"Warrant"};
	ranks[2] = rank3;
	char* rank4 = new char[N] {"Lieutenant"};
	ranks[3] = rank4;
	char* rank5 = new char[N] {"Colonel"};
	ranks[4] = rank5;
	char* rank6 = new char[N] {"General"};
	ranks[5] = rank6;

	int index1 = -1, index2 = -1;
	for (int i = 0; i < M; i++)
	{
		if (index1 != -1 && strcmp(ranks[i], firstRank) == 0) {
			index1 = i;
		}

		if (index2 != -1 && strcmp(ranks[i], secondRank) == 0) {
			index2 = i;
		}
	}

	return index1 < index2;
}

void displayBinaryFile(char* fileName)
{
	ifstream streamIn(fileName, ios::binary);
	if (!streamIn.is_open())
	{
		cout << "Can't open file to read!";
		SystemFun();
		return;
	}
	int bufSize = sizeof(Soldier);
	Soldier* soldier = new Soldier;
	while (streamIn.read((char*)soldier, bufSize))
	{
		soldier->displaySoldier();
	}
	streamIn.close();
}

void InitNumber(long& n)
{
	cout << "Enter the number of record:" << endl;
	cin >> n;
	system("cls");
}

void SystemFun()
{
	cout << endl;
	system("pause");
	system("cls");
}

void InitNameFile(char* fileName)
{
	cout << "Enter the name of file: " << endl;
	cin.ignore();
	cin.getline(fileName, 256, '\n');
	system("cls");
}

int Menu()
{
	int k;
	cout << "\n Enter the number - the mode of operations with file:"
		"\n 1 - INITIALIZATION THE NAME OF FILE"
		"\n 2 - FORMATION OF THE FILE"
		"\n 3 - VIEWING OF CONTENTS OF THE FILE"
		"\n 4 - ADD AN ELEMENT TO AND OF FILE"
		"\n 5 - CHANGE THE ELEMENT IN THE FILE"
		"\n 6 - REMOVE THE ELEMENT IN THE FILE"
		"\n 7 - SORTING THE ELEMENT IN THE FILE"
		"\n 8 - EXIT\n";
	cin >> k;
	return k;
}

Soldier initSoldier()
{
	char firstName[N] = "", secondName[N] = "", nationality[N] = "", rank[N] = "";
	int day, month, year;
	cout << "\nEnter first name:";
	cin.ignore();
	cin.get(firstName, N, '\n');
	cout << "Enter second name:";
	cin.ignore();
	cin.get(secondName, N, '\n');
	cout << "Enter Nationality:";
	cin.ignore();
	cin.get(nationality, N, '\n');
	cout << "Enter Rank:";
	cin.ignore();
	cin.get(rank, N, '\n');

	cout << "Enter the year of birth: ";
	cin >> year;
	cout << "Enter the month of birth: ";
	cin >> month;
	cout << "Enter the day of birth: ";
	cin >> day;
	Soldier person;
	person.setSoldier(firstName, secondName, nationality, rank, day, month, year);

	int hasAdress;
	cout << "Does the soldier have a place of residence?" << endl;
	cout << "Enter 1 to say yes ||| Enter 0 to say no" << endl;
	cin >> hasAdress;
	cout << endl;

	if (hasAdress) {
		int postIndex, house, flat;
		char country[N] = "", region[N] = "", district[N] = "", city[N] = "", street[N] = "";
		cout << "Enter the post index: ";
		cin >> postIndex;
		cout << endl;
		cout << "Enter Country:";
		cin.ignore();
		cin.get(country, N, '\n');
		cout << "Enter Region:";
		cin.ignore();
		cin.get(region, N, '\n');
		cout << "Enter District:";
		cin.ignore();
		cin.get(district, N, '\n');
		cout << "Enter City:";
		cin.ignore();
		cin.get(city, N, '\n');
		cout << "Enter Street:";
		cin.ignore();
		cin.get(street, N, '\n');

		cout << endl;
		cout << "Enter the house: ";
		cin >> house;
		cout << endl;
		cout << "Enter the flat: ";
		cin >> flat;
		cout << endl;

		person.setAdress(postIndex, country, region, district, city, street, house, flat);
	}

	return person;
}
 
// 
//#include <iostream>
// #include "Adress.h"
//#include "soldier.h"
//
//using namespace std;
//
//Soldier* initSoldiers(int);
//Soldier initSoldier();
//void displaySoldiers(Soldier*, int);
//void displayChoise(Soldier*, int, char*, int, int);
//void sortNationality(Soldier*, int);
//void swap(Soldier&, Soldier&);
//
//const int N = 20;
//
//int main() {
//
//	int n;
//	cout << "\nEnter the number of Soldiers:";
//	cin >> n;
//	system("cls");
//
//	Soldier* soldiers = initSoldiers(n);
//	if (!soldiers)
//	{
//		cout << "\nDynamic array doesn't exist!\n";
//		system("pause");
//		return 0;
//	}
//
//	cout << "\nThe list of Soldiers:\n";
//	displaySoldiers(soldiers, n);
//	char rankTag[N];
//	cout << "\nEnter the Rank-tag:";
//	cin >> rankTag;
//	int lowYear, upperYear;
//	cout << "\nEnter the low boundary of Year:";
//	cin >> lowYear;
//	cout << "\nEnter the upper boundary of Year:";
//	cin >> upperYear;
//	cout << "\n\nThe list of choise-Soldiers:\n";
//	displayChoise(soldiers, n, rankTag, lowYear, upperYear);
//	cout << "\n\nThe sorting list of Soldiers: \n";
//	sortNationality(soldiers, n);
//	displaySoldiers(soldiers, n);
//	delete[] soldiers;
//	system("pause");
//	return 0;
//}
//
//Soldier* initSoldiers(int n)
//{
//	Soldier* array = new Soldier[n];
//	if (!array)
//		return NULL;
//	for (int i = 0; i < n; i++)
//	{
//		cout << "\nEnter the information about " << (i + 1) << " Soldier\n";
//		array[i] = initSoldier();
//		system("cls");
//	}
//	return array;
//}
//
//Soldier initSoldier()
//{
//	char firstName[N] = "", secondName[N] = "", nationality[N] = "", rank[N] = "";
//	int day, month, year;
//	cout << "\nEnter first name:";
//	cin.ignore();
//	cin.get(firstName, N, '\n');
//	cout << "Enter second name:";
//	cin.ignore();
//	cin.get(secondName, N, '\n');
//	cout << "Enter Nationality:";
//	cin.ignore();
//	cin.get(nationality, N, '\n');
//	cout << "Enter Rank:";
//	cin.ignore();
//	cin.get(rank, N, '\n');
//
//	cout << "Enter the year of birth: ";
//	cin >> year;
//	cout << endl;
//	cout << "Enter the month of birth: ";
//	cin >> month;
//	cout << endl;
//	cout << "Enter the day of birth: ";
//	cin >> day;
//	cout << endl;
//	Soldier person;
//	person.setSoldier(firstName, secondName, nationality, rank, day, month, year);
//
//	int hasAdress;
//	cout << "Does the soldier have a place of residence?" << endl;
//	cout << "Enter 1 to say yes ||| Enter 0 to say no" << endl;
//	cin >> hasAdress;
//	cout << endl;
//
//	if (hasAdress) {
//		int postIndex, house, flat;
//		char country[N] = "", region[N] = "", district[N] = "", city[N] = "", street[N] = "";
//		cout << "Enter the post index: ";
//		cin >> postIndex;
//		cout << endl;
//		cout << "Enter Country:";
//		cin.ignore();
//		cin.get(country, N, '\n');
//		cout << "Enter Region:";
//		cin.ignore();
//		cin.get(region, N, '\n');
//		cout << "Enter District:";
//		cin.ignore();
//		cin.get(district, N, '\n');
//		cout << "Enter City:";
//		cin.ignore();
//		cin.get(city, N, '\n');
//		cout << "Enter Street:";
//		cin.ignore();
//		cin.get(street, N, '\n');
//
//		cout << endl;
//		cout << "Enter the house: ";
//		cin >> house;
//		cout << endl;
//		cout << "Enter the flat: ";
//		cin >> flat;
//		cout << endl;
//
//		person.setAdress(postIndex, country, region, district, city, street, house, flat);
//	}
//
//	return person;
//}
//
//void displaySoldiers(Soldier* array, int n)
//{
//	for (int i = 0; i < n; i++)
//		array[i].displaySoldier();
//	cout << endl;
//}
//
//
//void displayChoise(Soldier* array, int Dimension, char* rankTag, int lowYear, int upperYear)
//{
//	for (int i = 0; i < Dimension; i++)
//		if (!strcmp(array[i].getRank(), rankTag) && array[i].getYear()
//			<= upperYear && array[i].getYear() >= lowYear)
//			array[i].displaySoldier();
//}
//
//
//void sortNationality(Soldier* array, int n)
//{
//	for (int i = 0; i <= n; i++)
//		for (int j = n - 1; j > i; j--)
//			if (strcmp(array[j].getNationality(), array[j - 1].getNationality()) < 0)
//				swap(array[j], array[j - 1]);
//}
//
//void swap(Soldier& a, Soldier& b)
//{
//	Soldier help = a;
//	a = b;
//	b = help;
//}
